﻿using ISMApi.mks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISMApi
{
   
    class Program
    {
        private static  FRSHEATIntegration ws = new FRSHEATIntegration();
        private const string user = "zabbix";
        private const string pwd = "Operator2019";
        private const string tenant = "SD-MKS";
        private const string role = "ServiceDeskAnalyst";
        static void Main(string[] args)
        {
        // Autentikace a ziskani session key
        //údaje zaměnit podle dokumentu
            //jmeno:
            //  zabbix
            //heslo:
            // Operator2019
            //role:
            // ServiceDeskAnalyst
            //tenant:
            //  SD-MKS
            FRSHEATIntegrationConnectionResponse session = ws.Connect(user,pwd,tenant,role);
            Console.WriteLine(session.sessionKey);
            

            // Vytvoření objektu pro vložení dat
            ObjectCommandData cmd = new ObjectCommandData();
            cmd.ObjectType = "Incident#";
            var test = new List<ObjectCommandDataFieldValue>()
            {
                //Status
                new ObjectCommandDataFieldValue()
                {
                    Name="Status",
                    Value="Logged"
                },
                new ObjectCommandDataFieldValue()
                {
                    //Subject = Trigger: Interface gi1/1/38(102 BO PC): Ethernet has changed to lower speed than it was before
                    Name="Subject",
                    Value="Interface gi1/1/38(102 BO PC): Ethernet has changed to lower speed than it was before"
                },
                new ObjectCommandDataFieldValue()
                {
                    //Symptom = Trigger Description: Last value: 10 Mbps. This Ethernet connection has transitioned down from its known maximum speed. This might be a sign of autonegotiation issues. Ack to close.
                    Name="Symptom",
                    Value="Interface gi1/1/38(102 BO PC): Ethernet has changed to lower speed than it was before"
                },
                 new ObjectCommandDataFieldValue()
                {
                     // Neměnit
                    Name="Urgency",
                    Value="medium"
                },
                 new ObjectCommandDataFieldValue()
                {
                     //Neměnit
                    Name="Impact",
                    Value="High"
                },
                new ObjectCommandDataFieldValue()
                {
                    //Neměnit Source je zdroj odkud je incident zaližen
                    Name="Source",
                    Value="Network Monitor",
                    BinaryData=null,
                    HtmlValue=null
                }
            };

            cmd.Fields = test.ToArray();
            SearchCondition search = new SearchCondition();

            // Přidání customera k incidentu povinné pole neměnit
            cmd.LinkToExistent = new List<LinkEntry>()
            {
                new LinkEntry()
                {
                    Action="link",
                    Relation="IncidentAssociatedCustomer",
                    RelationshipName="IncidentAssociatedCustomer",
                    RelatedObjectId="8119352D8F4B48D3BFCBADDF59AB29E8",
                    RelatedObjectType="Employee#"
                }


            }.ToArray();

            //Search criteria - podle toho se pozná, jestli má objekt vytvořit nebo změnit existující
            string[] testik = {"Subject"};
            var resp=ws.UpsertObject(session.sessionKey,tenant,cmd, testik);
            Console.WriteLine(resp.status + " " + resp.recId + " " + resp.exceptionReason);
            Console.ReadKey();
        }
    }
}

